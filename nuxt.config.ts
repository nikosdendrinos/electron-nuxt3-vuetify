// https://nuxt.com/docs/api/configuration/nuxt-config
import type { ElectronOptions } from 'nuxt-electron'


export default defineNuxtConfig({
  modules: [
    ['nuxt-electron', <ElectronOptions>{
      include: ['electron'],
    }],
  ],
  vite: {
    define: {
      'process.env.DEBUG': false
    }
  },
  css: [
    'vuetify/lib/styles/main.sass',
  ],
  build:{
    transpile: ['vuetify']
  },
})
